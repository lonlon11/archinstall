#!/bin/bash

###########################################################################
##### Dotfiles [https://www.atlassian.com/git/tutorials/dotfiles]
###########################################################################

rm -rf .zshrc .config/htop .config/kitty .config/nvim 
echo ".cfg" >> .gitignore
git clone --bare https://oauth2:<Access Token for minimal_dotfiles git repo>@gitlab.com/lonlon11/minimal_dotfiles.git $HOME/.cfg

/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME checkout

rm -rf .cfg .gitignore

# Get keys
gpg --recv-keys D8714CE819C23197EA9B26F8550A83C6ED6E5277