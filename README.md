# Arch Install Scripts

## Reinstallation of my laptop
1. Use `archinstall` to install the base system
2. Clone this repository
   ```
   git clone https://gitlab.com/lonlon11/archinstall.git /tmp/archinstall
   cd /tmp/archinstall/laptop
   ```
3. Create an access token to the dotfiles repo on GitLab with `Role: Reporter` and `Scope: read_api; read_repository`
4. Check if all packets from packet_list exist
   ```
   chmod +x check_package_list.sh
   ./check_package_list.sh
   ```   
5. Run the installation script (as a user)
   ```
   chmod +x install.sh
   ./install.sh TOKEN
   ```
5. Run the configuration script for the eduroam network
   ```
   chmod +x eduroam-linux-SFIoTZ-eduroam_ETH_Zurich.py
   ./eduroam-linux-SFIoTZ-eduroam_ETH_Zurich.py
   ```

## This whole section regarding "minimal installation" needs to be adjusted!
<del>
## Installation of the minimal dotfiles
1. Clone this repository into your home directory
2. Install the necessary fonts and applications

   Install FiraCode Font
   ```
   https://github.com/tonsky/FiraCode/wiki/Linux-instructions#installing-with-a-package-manager
   ```

   Install Kitty
   ```
   https://github.com/kovidgoyal/kitty
   ```

   Install htop
   ```
   https://github.com/htop-dev/htop
   ```

   Install nvim
   ```
   https://github.com/neovim/neovim/wiki/Installing-Neovim   
   ```
3. Install ZSH
   ```
   https://github.com/ohmyzsh/ohmyzsh/wiki/Installing-ZSH/
   ```
   Make it the default shell
   ```
   chsh -s /bin/zsh $USER
   ```
   Install OhMyZSH
   ```
   chmod +x zsh.sh
   ./zsh.sh
   ```
4. Install the dotfiles

   Create an access token to the minimal_dotfiles repo on gitlab and add this token as `<Access Token for minimal_dotfiles git repo>` to `minimal_dotfiles.sh`
   ```
   chmod +x minimal_dotfiles.sh
   ./dotfiles.sh
   ```
</del>