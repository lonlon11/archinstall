#!/bin/bash



###########################################################################
# This script is for the installation of my private laptop.
# It assumes that my dotfiles are stored on GitLab.
# The variables that might change are the ones below. Everything else probably does not need to be modified too often.  
###########################################################################

GITLAB_USERNAME="lonlon11"
GITLAB_EMAIL="leonwindler@gmail.com"
DOTFILES_REPO="dotfiles"

GPG_KEY="D8714CE819C23197EA9B26F8550A83C6ED6E5277"

MOUNTING_POINT_DATA="/data"



###########################################################################
# ATTENTION: this script must be run as a user and not as root! To switch from root to user XYZ use:    
###########################################################################
#     su XYZ

# Get the current username of the user running the script
USERNAME="$USER"

# Check if the script is being run as root (su)
if [ "$EUID" -eq 0 ]; then
    echo -e "\e[31mERROR: This script must be run as a regular user, not as root (su).\e[0m"
    echo -e "\e[33mUsage: su $USERNAME\e[0m"
    exit 1
fi



###########################################################################
# Make sure that the file package_list exists
###########################################################################

# Check if the package_list file exists
if [ ! -f "package_list" ]; then
    echo -e "\e[31mERROR: File package_list not found.\e[0m"
    exit 1
fi



###########################################################################
# The access token for the dotfiles GitLab repo must have the following properties:
###########################################################################
#     Role:       Reporter
#     Scopes:     read_api; read_repository

# Check if the TOKEN argument is provided
if [ -z "$1" ]; then
    echo -e "\e[31mERROR: Please provide the GitLab Access Token as an argument when calling the script.\e[0m"
    echo -e "\e[33mUsage: $0 TOKEN\e[0m"
    exit 1
fi

# Assign the provided token to the variable
GITLAB_ACCESS_TOKEN="$1"

URL="https://gitlab.com/api/v4/users/$GITLAB_USERNAME/projects"
HEADERS="Authorization: Bearer $GITLAB_ACCESS_TOKEN"

RESPONSE=$(curl -s -H "$HEADERS" "$URL")

# Check if the access token is valid
if [[ "$RESPONSE" =~ "\"message\":\"401 Unauthorized\"" ]]; then
  echo -e "\e[31mERROR: The GitLab Access Token is invalid.\e[0m"
  exit 1
else
  echo -e "\e[32mValid GitLab Access Token.\e[0m"
fi



###########################################################################
# The files for the symlinks are assumed to be at /data
###########################################################################
#     Size          Mounting Point      Format
#       1 GB        /boot               FAT32
#     xxx GB        /                   EXT4
#     yyy GB        /data               EXT4

# Function to prompt for valid yes/no input and store in a variable
prompt_symlinks() {
    echo -en "\e[33m" # Set the text color to yellow
    read -rp "Do you want to add the symbolic links at the end? (yes/no): " RESPONSE
    echo -en "\e[0m"  # Reset the text color to default
    case "$RESPONSE" in
        [yY]|[yY][eE][sS]) INSTALL_SYMLINKS=true ;;
        [nN]|[nN][oO]) INSTALL_SYMLINKS=false ;;
        *) echo -e "\e[31mERROR: Please enter a valid response (yes/no).\e[0m" ; prompt_symlinks ;;
    esac
}

# Prompt the user if they want to add symlinks at the end
prompt_symlinks

if $INSTALL_SYMLINKS; then
    # Function to check if a partition is mounted
    check_partition_mounted() {
        PARTITION=$1
        if ! grep -qs "$PARTITION" /proc/mounts; then
            echo -e "\e[31mERROR: Partition $PARTITION is not mounted.\e[0m"
            exit 1
        fi
    }

    # Check if required partitions are mounted
    # check_partition_mounted "/boot"
    # check_partition_mounted "/"
    check_partition_mounted "$MOUNTING_POINT_DATA"
fi



###########################################################################
##### Laptop install?
###########################################################################

# Function to prompt for valid yes/no input and store in a variable
prompt_laptop() {
    echo -en "\e[33m" # Set the text color to yellow
    read -rp "Do you want to install a laptop (T490)? (yes/no): " RESPONSE
    echo -en "\e[0m"  # Reset the text color to default
    case "$RESPONSE" in
        [yY]|[yY][eE][sS]) INSTALL_LAPTOP=true ;;
        [nN]|[nN][oO]) INSTALL_LAPTOP=false ;;
        *) echo -e "\e[31mERROR: Please enter a valid response (yes/no).\e[0m" ; prompt_laptop ;;
    esac
}

# Prompt the user if they want to install a laptop (T490)
prompt_laptop



###########################################################################
##### Start the installation
###########################################################################

# Passed all preliminary tests, the setup can begin.
echo ""
echo -e "\e[30;42mAll prerequisites are met\!\n\nStarting the installation\!\e[0m"
echo""



###########################################################################
##### Packages
###########################################################################

# Requirement for the installation of yay
if ! pacman -Q git &> /dev/null; then
    echo -e "\e[34mGit package is not installed. Installing...\e[0m"
    sudo pacman -S git
else
    echo -e "\e[32mGit package is already installed.\e[0m"
fi

# Install yay
echo -e "\e[34mInstalling yay...\e[0m" 
git clone https://aur.archlinux.org/yay.git /tmp/yay
(cd /tmp/yay/ && makepkg -si --noconfirm)

# Import Dropbox's public key
echo -e "\e[35mImport Dropbox's public key...\e[0m" 
curl https://linux.dropbox.com/fedora/rpm-public-key.asc --output /tmp/rpm-public-key.asc
gpg --import /tmp/rpm-public-key.asc
rm /tmp/rpm-public-key.asc

# Install all packages 
echo -e "\e[34mInstalling package list...\e[0m" 
# Read packages from the package_list file and install them using yay
xargs -a $PWD/package_list yay -S --needed



###########################################################################
##### ZSH
###########################################################################

# Install OhMyZSH
echo -e "\e[34mInstalling OhMyZSH...\e[0m" 
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended

# Installing zsh-autosuggestions
echo -e "\e[34mInstalling zsh-autosuggestions...\e[0m" 
git clone https://github.com/zsh-users/zsh-autosuggestions.git $HOME/.oh-my-zsh/custom/plugins/zsh-autosuggestions

# Installing zsh-syntax-highlighting
echo -e "\e[34mInstalling zsh-syntax-highlighting...\e[0m" 
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git $HOME/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting

# Installing powerlevel10k
echo -e "\e[34mInstalling powerlevel10k...\e[0m" 
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git $HOME/.oh-my-zsh/custom/themes/powerlevel10k



###########################################################################
##### Dotfiles [https://www.atlassian.com/git/tutorials/dotfiles]
###########################################################################

# Import dot files
echo -e "\e[35mImport dot files...\e[0m"
rm -rf .zshrc .config
echo ".cfg" >> .gitignore
git clone --bare "https://oauth2:$GITLAB_ACCESS_TOKEN@gitlab.com/$GITLAB_USERNAME/$DOTFILES_REPO.git" "$HOME/.cfg"

# Checkout the dot files
/usr/bin/git --git-dir="$HOME/.cfg/" --work-tree="$HOME" checkout
/usr/bin/git --git-dir="$HOME/.cfg/" --work-tree="$HOME" config --local status.showUntrackedFiles no

# Add the email and change remote from HTTPS to SSH
/usr/bin/git --git-dir="$HOME/.cfg/" --work-tree="$HOME" config user.email "$GITLAB_EMAIL"
/usr/bin/git --git-dir="$HOME/.cfg/" --work-tree="$HOME" remote set-url origin "git@gitlab.com:$GITLAB_USERNAME/$DOTFILES_REPO.git"

source $HOME/.zshrc



###########################################################################
##### Configurations
###########################################################################

# Add the user to all necessary groups
echo -e "\e[35mAdd the user to all necessary groups...\e[0m" 
sudo usermod -a -G audio,input,libvirt,log,video,wheel -s /bin/zsh $USERNAME

# Activate F11 key (Lenovo T490) [also inserts pacman configuration]
echo -e "\e[35mImport configuration files...\e[0m" 
sudo rsync -p -o -g -r $PWD/files/etc/ /etc

if $INSTALL_LAPTOP; then
    # Starting libinput-gestures
    echo -e "\e[35mEnable libinput-gestures...\e[0m" 
    libinput-gestures-setup autostart start
else
    # Remove files I only need on the laptop
    rm /etc/X11/xorg.conf.d/30-touchpad.conf
    rm /etc/udev/hwdb.d/90-thinkpad-keyboard.hwdb
fi

sudo chown root:root /etc
sudo systemd-hwdb update 
sudo udevadm trigger --sysname-match="event*"

# Enable lockscreen activation upon suspension
echo -e "\e[35mEnable betterlockscreen...\e[0m" 
systemctl enable betterlockscreen@$USER --now

# Enable TeamViewer daemon
echo -e "\e[35mEnable TeamViewer daemon...\e[0m" 
systemctl enable teamviewerd.service --now

# Enable cronie
echo -e "\e[35mEnable cronie...\e[0m" 
sudo systemctl enable cronie.service --now



###########################################################################
##### GPG / SSH
###########################################################################

# For setting up new Yubikeys
# [https://github.com/drduh/YubiKey-Guide]
# [https://developer.okta.com/blog/2021/07/07/developers-guide-to-gpg]

# When the Yubikeys are already set up
# 1) Insert yubikey and run 'gpg --card-edit' followed by 'fetch' to import the public key
# 2) Insert the configuration files '.gnupg/gpg.conf' and '.gnupg/gpg-agent.conf'
# 3) Have the configurations set in zshrc
# 4) Reload the gpg agent with 'gpg-connect-agent reloadagent /bye'

# When the touch capability is requested, there is a notification program for this including a polybar module.
# See: https://github.com/maximbaz/yubikey-touch-detector

# Import my public key
echo -e "\e[35mImport my GPG public key...\e[0m" 
gpg --recv-keys "$GPG_KEY"



###########################################################################
##### Lockscreen
###########################################################################

# Set betterlockscreen wallpaper
echo -e "\e[35mSet betterlockscreen wallpaper...\e[0m" 
betterlockscreen -u $HOME/.config/betterlockscreen_wallpaper.jpg



###########################################################################
##### Scripts
###########################################################################

# Make all scripts in $HOME/.scripts executable with
echo -e "\e[35mMake homemade scripts executable...\e[0m" 
chmod -R +x $HOME/.scripts/



###########################################################################
##### Dynamic Wallpaper [https://github.com/adi1090x/dynamic-wallpaper]
###########################################################################

echo -e "\e[35mSetup dynamic wallpaper...\e[0m" 
crontab -l > mycron
echo "0 * * * * env PATH=$PATH DISPLAY=$DISPLAY DESKTOP_SESSION=$DESKTOP_SESSION DBUS_SESSION_BUS_ADDRESS=\"$DBUS_SESSION_BUS_ADDRESS\" /usr/bin/dwall -s beach" >> mycron
crontab mycron
rm mycron



###########################################################################
##### Data Partition [https://community.linuxmint.com/tutorial/view/1609]
###########################################################################

# This needs to be done if the partitions are not already mounted in the installation (assume I want to mount at /mnt/data):

# sudo mkdir /mnt/data
# sudo mount /dev/nvme0n1p7 /mnt/data
# sudo chown -R $USERNAME: /mnt/data
# sudo nvim /etc/fstab
#     ...
#     # Data partition
#     # /dev/nvme0n1p7
#     UUID=479199f0-19bf-4cd2-9978-eb72078ef0c2       /mnt/data       ext4    defaults        0 2

# This assumes that the data partition was mounted at /data during the installation

if $INSTALL_SYMLINKS; then
    echo -e "\e[35mMake symbolic links into home directory...\e[0m" 
    # Function to create symbolic link and delete destination if it already exists
    create_and_link() {
        source_dir=$1
        dest_dir=$2

        if [ -e "$dest_dir" ]; then
            echo -e "\e[33mDestination already exists, deleting: $dest_dir\e[0m"
            rm -rf "$dest_dir"
        else
            echo -e "\e[32mDestination does not exist."
            rm -rf "$dest_dir"
        fi

        echo -e "\e[34mCreating symbolic link: $source_dir -> $dest_dir\e[0m"
        ln -s "$source_dir" "$dest_dir"
    }

    # Create symbolic links for all folders in $MOUNTING_POINT_DATA (inclusive the ones that start with ".")
    # If a folder already exists in the destination, delete it and create a new symlink
    for dir in $MOUNTING_POINT_DATA/* $MOUNTING_POINT_DATA/.*; do
        dir_name=$(basename "$dir")
        # Skip certain directories
        if [ "$dir_name" != "lost+found" ] && [ "$dir_name" != ".Trash-1000" ] && [ "$dir_name" != "." ] && [ "$dir_name" != ".." ]; then
            # Create symbolic link for the directory
            create_and_link "$dir" "$HOME/$dir_name"
        fi
    done
fi



############################################################################
##### Configure Cloudflare WARP client
###########################################################################

# To get more information: https://developers.cloudflare.com/warp-client/get-started/linux/
if yay -Q cloudflare-warp-bin >/dev/null 2>&1; then
    # Register the client 
    warp-cli register

    # Enroll the organization
    warp-cli teams-enroll lonlon 
fi


##########################################################################
##### GPU
###########################################################################

# Figure out how to reliably disable the GPU
# https://github.com/boogy/dotfiles/blob/master/deploy/archlinux/README.md
# https://wiki.archlinux.org/title/Lenovo_ThinkPad_T490#Extremely_high_battery_usage_because_of_dedicated_GPU
