#!/bin/bash

# Check if yay is installed
if ! command -v yay &> /dev/null; then
    echo "Error: yay not found. Please install yay first."
    exit 1
fi

# Check if the package_list file exists
if [ ! -f "package_list" ]; then
    echo "Error: package_list file not found."
    exit 1
fi

# Read packages from the package_list file and check if they exist in the repositories
yay -Syy

not_found=()
while read -r package; do
  echo "Checking: $package"
    if ! yay -Qs "$package" &> /dev/null; then
        not_found+=("$package")
    fi
done < package_list

# Print the list of packages not found
echo ""
if [ ${#not_found[@]} -eq 0 ]; then
  echo -e "\e[32mAll packages found in repositories.\e[0m"
else
  echo "Packages not found in repositories:"
  printf '\e[31m%s\n\e[0m' "${not_found[@]}"
fi
